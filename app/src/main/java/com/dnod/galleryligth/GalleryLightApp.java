package com.dnod.galleryligth;

import android.app.Application;
import android.app.FragmentManager;
import android.arch.persistence.room.Room;

import com.dnod.galleryligth.data.source.DataBase;

public class GalleryLightApp extends Application {
    private static final String DB_NAME = "gallery_light_bd";

    private static GalleryLightApp sInstance;

    public static GalleryLightApp getInstance() {
        return sInstance;
    }

    private DataBase mDataBase;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        FragmentManager.enableDebugLogging(BuildConfig.DEBUG);
        mDataBase = Room.databaseBuilder(this, DataBase.class, DB_NAME).build();
    }

    public static DataBase getDataBase() {
        return sInstance.mDataBase;
    }
}
