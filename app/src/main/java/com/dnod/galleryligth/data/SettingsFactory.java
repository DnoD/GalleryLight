package com.dnod.galleryligth.data;

import com.dnod.galleryligth.utils.Constants;

public final class SettingsFactory {

    public static Settings createDefaultSettings() {
        Settings settings = new Settings();
        settings.setName(Constants.SETTINGS_NAME);
        settings.setFileExtensions("");
        settings.setGalleryColumnsValue(3);
        settings.setImportLimit(1);
        settings.setPagingSize(15);
        return settings;
    }
}
