package com.dnod.galleryligth.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Settings {
    @PrimaryKey
    @NonNull
    private String name;

    private String fileExtensions;
    private int importLimit;
    private int pagingSize;
    private int galleryColumnsValue;

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public String getFileExtensions() {
        return fileExtensions;
    }

    public void setFileExtensions(String fileExtensions) {
        this.fileExtensions = fileExtensions;
    }

    public int getImportLimit() {
        return importLimit;
    }

    public void setImportLimit(int importLimit) {
        this.importLimit = importLimit;
    }

    public int getPagingSize() {
        return pagingSize;
    }

    public void setPagingSize(int pagingSize) {
        this.pagingSize = pagingSize;
    }

    public int getGalleryColumnsValue() {
        return galleryColumnsValue;
    }

    public void setGalleryColumnsValue(int galleryColumnsValue) {
        this.galleryColumnsValue = galleryColumnsValue;
    }
}
