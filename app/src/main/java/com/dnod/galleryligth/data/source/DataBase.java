package com.dnod.galleryligth.data.source;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.dnod.galleryligth.data.Image;
import com.dnod.galleryligth.data.Settings;
import com.dnod.galleryligth.data.dao.ImageDao;
import com.dnod.galleryligth.data.dao.SettingsDao;

@Database(entities = {Settings.class, Image.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class DataBase extends RoomDatabase {

    public abstract SettingsDao getSettingsDao();

    public abstract ImageDao getImageDao();
}
