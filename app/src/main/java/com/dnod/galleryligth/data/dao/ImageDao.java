package com.dnod.galleryligth.data.dao;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.dnod.galleryligth.data.Image;

import java.util.List;

@Dao
public interface ImageDao {

    @Insert
    void insert(List<Image> images);

    @Delete
    void delete(List<Image> images);

    @Query("SELECT * FROM image")
    List<Image> getImages();

    @Query("SELECT * FROM image ORDER BY date ASC")
    DataSource.Factory<Integer, Image> getImagesByDate();

    @Query("DELETE FROM image")
    void clear();
}
