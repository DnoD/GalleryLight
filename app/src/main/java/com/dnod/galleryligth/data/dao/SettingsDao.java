package com.dnod.galleryligth.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.dnod.galleryligth.data.Settings;

import java.util.List;

@Dao
public interface SettingsDao {

    @Insert
    void insert(Settings settings);

    @Update
    void update(Settings settings);

    @Delete
    void delete(Settings settings);

    @Query("SELECT * FROM settings WHERE name=:name")
    List<Settings> getSettings(String name);
}
