package com.dnod.galleryligth.utils;

public interface Constants {
    int REQUEST_PICK_IMAGES = 0;
    int REQUEST_CHECK_APP_PERMISSIONS = 1;

    int EMPTY_INT_VALUE = -1;
    long EMPTY_LONG_VALUE = -1L;

    String SETTINGS_NAME = "default_settings";

    String PNG_EXTENSION = "png";
    String JPEG_EXTENSION = "jpeg";
}
