package com.dnod.galleryligth.ui.gallery;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dnod.galleryligth.R;
import com.dnod.galleryligth.data.Image;
import com.dnod.galleryligth.databinding.FragmentGalleryBinding;
import com.dnod.galleryligth.service.DeleteImagesService;
import com.dnod.galleryligth.ui.BaseFragment;
import com.dnod.galleryligth.ui.MainActivity;

import java.util.ArrayList;

public class GalleryFragment extends BaseFragment implements GalleryAdapter.Listener {

    public static BaseFragment createInstance() {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private ActionMode mActionMode;
    private GalleryViewModel mViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentGalleryBinding bindingObject = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery, container, false);
        mViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
        mViewModel.setBindingObject(bindingObject);
        mViewModel.setGalleryListener(this);
        return bindingObject.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            menu.clear();
        }
        inflater.inflate(R.menu.gallery, menu);
        menu.findItem(R.id.none).setChecked(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        switch (item.getItemId()) {
            case R.id.none:
                mViewModel.updateGalleryColumns();
                handled = true;
                break;
            case R.id.single:
                mViewModel.updateGalleryColumns(1);
                handled = true;
                break;
            case R.id.two:
                mViewModel.updateGalleryColumns(2);
                handled = true;
                break;
            case R.id.three:
                mViewModel.updateGalleryColumns(3);
                handled = true;
                break;
        }
        if (handled) {
            item.setChecked(true);
        }
        return handled;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewModel.setBindingObject(null);
    }

    @Override
    public String getScreenTag() {
        return GalleryFragment.class.getSimpleName();
    }

    @Override
    public void onSelectionListener(int count) {
        if (count > 0) {
            if (mActionMode != null) {
                return;
            }
            ((MainActivity)getActivity()).getScreenToolbar().startActionMode(new ActionMode.Callback() {
                @Override
                public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                    mActionMode = actionMode;
                    actionMode.getMenuInflater().inflate(R.menu.gallery_action_mode, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                    if (menuItem.getItemId() == R.id.delete) {
                        getActivity().startService(DeleteImagesService.createIntent(getContext(),
                                new ArrayList<>(mViewModel.getSelectedItems())));
                        mActionMode.finish();
                        return true;
                    }
                    return false;
                }

                @Override
                public void onDestroyActionMode(ActionMode actionMode) {
                    mActionMode = null;
                    mViewModel.resetSelection();
                }
            });
        } else {
            mActionMode.finish();
        }
    }
}
