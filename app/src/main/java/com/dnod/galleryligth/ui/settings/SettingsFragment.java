package com.dnod.galleryligth.ui.settings;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dnod.galleryligth.R;
import com.dnod.galleryligth.databinding.FragmentSettingsBinding;
import com.dnod.galleryligth.service.ClearDBService;
import com.dnod.galleryligth.service.ImportService;
import com.dnod.galleryligth.ui.BaseFragment;
import com.dnod.galleryligth.utils.Constants;

import java.util.ArrayList;

public class SettingsFragment extends BaseFragment implements SettingsViewModel.Listener {

    public static BaseFragment createInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private SettingsViewModel mViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewModel = ViewModelProviders.of(this).get(SettingsViewModel.class);
        FragmentSettingsBinding bindingObject = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        mViewModel.setBindingObject(bindingObject);
        mViewModel.setListener(this);
        return bindingObject.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.settings, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                mViewModel.save();
                return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_PICK_IMAGES && resultCode == Activity.RESULT_OK) {
            ArrayList<Uri> images = new ArrayList<>();
            if (data.getClipData() != null) {
                if (data.getClipData().getItemCount() > mViewModel.getSettings().getImportLimit()) {
                    Snackbar.make(getView(), getString(R.string.error_exceed_files_limit,
                            mViewModel.getSettings().getImportLimit()), Snackbar.LENGTH_LONG).show();
                    return;
                }
                for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                    ClipData.Item item = data.getClipData().getItemAt(i);
                    images.add(item.getUri());
                }
            } else {
                images.add(data.getData());
            }
            getActivity().startService(ImportService.createIntent(getContext(), images));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewModel.setBindingObject(null);
    }

    @Override
    public String getScreenTag() {
        return SettingsFragment.class.getSimpleName();
    }

    @Override
    public void onImportClicked() {
        sendImagesIntent();
    }

    @Override
    public void onClearClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.message_clear_db)).setPositiveButton(R.string.btn_remove, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                getActivity().startService(ClearDBService.createIntent(getContext()));
            }
        }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setCancelable(true).create().show();
    }

    private void sendImagesIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        if (!mViewModel.getFileExtensions().isEmpty()) {
            String[] extensions = mViewModel.getFileExtensions().split("\\|");
            String[] mimeTypes = new String[extensions.length];
            for (int i = 0; i < extensions.length; i++) {
                mimeTypes[i] = "image/" + extensions[i];
            }
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_pick_images)), Constants.REQUEST_PICK_IMAGES);
    }
}
