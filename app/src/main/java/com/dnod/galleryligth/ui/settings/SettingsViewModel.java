package com.dnod.galleryligth.ui.settings;

import android.arch.lifecycle.ViewModel;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;

import com.dnod.galleryligth.GalleryLightApp;
import com.dnod.galleryligth.R;
import com.dnod.galleryligth.data.Settings;
import com.dnod.galleryligth.data.SettingsFactory;
import com.dnod.galleryligth.databinding.FragmentSettingsBinding;
import com.dnod.galleryligth.utils.Constants;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public final class SettingsViewModel extends ViewModel {

    public interface Listener {
        void onImportClicked();

        void onClearClicked();
    }

    private Listener mListener;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private FragmentSettingsBinding mBindingObject;
    private Settings mSettings;
    private String[] mPagingValues;
    private String[] mGalleryColumnsValues;
    private String[] mImportLimitsValues;

    Settings getSettings() {
        return mSettings;
    }

    void setListener(Listener listener) {
        this.mListener = listener;
    }

    void setBindingObject(FragmentSettingsBinding bindingObject) {
        this.mBindingObject = bindingObject;
        if (bindingObject != null) {
            mPagingValues = mBindingObject.getRoot().getContext()
                    .getResources().getStringArray(R.array.paging_values);
            mGalleryColumnsValues = mBindingObject.getRoot().getContext()
                    .getResources().getStringArray(R.array.gallery_columns_values);
            mImportLimitsValues = mBindingObject.getRoot().getContext()
                    .getResources().getStringArray(R.array.import_limits);
            init();
            applySettings();
            loadSettings();
        } else {
            compositeDisposable.clear();
        }
    }

    private void loadSettings() {
        compositeDisposable.add(Schedulers.io().scheduleDirect(new Runnable() {
            @Override
            public void run() {
                List<Settings> settings = GalleryLightApp.getDataBase()
                        .getSettingsDao().getSettings(Constants.SETTINGS_NAME);
                if (!settings.isEmpty()) {
                    mSettings = settings.get(0);
                } else {
                    mSettings = SettingsFactory.createDefaultSettings();
                    GalleryLightApp.getDataBase().getSettingsDao().insert(mSettings);
                }
                mBindingObject.setModel(SettingsViewModel.this);
                applySettings();
            }
        }));

    }

    private void applySettings() {
        if (mSettings == null) {
            return;
        }
        for (int i = 0; i < mPagingValues.length; i++) {
            if (mSettings.getPagingSize() == Integer.valueOf(mPagingValues[i])) {
                mBindingObject.pagingValue.setSelection(i);
                break;
            }
        }
        for (int i = 0; i < mGalleryColumnsValues.length; i++) {
            if (mSettings.getGalleryColumnsValue() == Integer.valueOf(mGalleryColumnsValues[i])) {
                mBindingObject.galleryColumnsValue.setSelection(i);
                break;
            }
        }
        for (int i = 0; i < mImportLimitsValues.length; i++) {
            if (mSettings.getImportLimit() == Integer.valueOf(mImportLimitsValues[i])) {
                mBindingObject.importLimitValue.setSelection(i);
                break;
            }
        }
    }

    private void init() {
        ArrayAdapter adapter;
        mBindingObject.pagingValue.setAdapter(adapter = ArrayAdapter.createFromResource(mBindingObject.getRoot().getContext(),
                R.array.paging_values, R.layout.simple_spinner_item));
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mBindingObject.galleryColumnsValue.setAdapter(adapter = ArrayAdapter.createFromResource(mBindingObject.getRoot().getContext(),
                R.array.gallery_columns_values, R.layout.simple_spinner_item));
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mBindingObject.importLimitValue.setAdapter(adapter = ArrayAdapter.createFromResource(mBindingObject.getRoot().getContext(),
                R.array.import_limits, R.layout.simple_spinner_item));
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mBindingObject.fileExtension.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mBindingObject.fileExtensionRoot.setError("");
            }
        });
    }

    public String getFileExtensions() {
        return mSettings.getFileExtensions();
    }

    public void onImportClick() {
        if (!validateForm()) {
            return;
        }
        save();
        if (mListener != null) {
            mListener.onImportClicked();
        }
    }

    public void onClearDbClick() {
        if (mListener != null) {
            mListener.onClearClicked();
        }
    }

    void save() {
        if (!validateForm()) {
            return;
        }
        mSettings.setImportLimit(Integer.valueOf(mImportLimitsValues[mBindingObject.importLimitValue.getSelectedItemPosition()]));
        mSettings.setGalleryColumnsValue(Integer.valueOf(mGalleryColumnsValues[mBindingObject.galleryColumnsValue.getSelectedItemPosition()]));
        mSettings.setPagingSize(Integer.valueOf(mPagingValues[mBindingObject.pagingValue.getSelectedItemPosition()]));
        mSettings.setFileExtensions(getExtensions());
        compositeDisposable.add(Schedulers.io().scheduleDirect(new Runnable() {
            @Override
            public void run() {
                GalleryLightApp.getDataBase().getSettingsDao().update(mSettings);
            }
        }));
    }

    private String getExtensions() {
        return mBindingObject.fileExtension.getText().toString().toLowerCase();
    }

    private boolean validateForm() {
        String extensions = mBindingObject.fileExtension.getText().toString().replaceAll(" ", "");
        if (!extensions.isEmpty()) {
            String[] extensionsArray = extensions.split("\\|");
            for (String extension : extensionsArray) {
                if (Constants.JPEG_EXTENSION.equals(extension.toLowerCase()) ||
                        Constants.PNG_EXTENSION.equals(extension.toLowerCase())) {
                    continue;
                }
                mBindingObject.fileExtensionRoot.setError(mBindingObject.getRoot().getContext()
                        .getString(R.string.error_not_supported_file_extension, extension));
                return false;
            }
        }
        return true;
    }
}
