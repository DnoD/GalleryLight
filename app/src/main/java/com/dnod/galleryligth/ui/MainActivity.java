package com.dnod.galleryligth.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.dnod.galleryligth.R;
import com.dnod.galleryligth.databinding.ActivityMainBinding;
import com.dnod.galleryligth.ui.dialog.ProgressDialog;
import com.dnod.galleryligth.ui.gallery.GalleryFragment;
import com.dnod.galleryligth.ui.settings.SettingsFragment;
import com.dnod.galleryligth.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (mCurrFragment != null && mBindingObject.navigation.getSelectedItemId() == item.getItemId()) {
                return false;
            }
            BaseFragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_gallery:
                    fragment = GalleryFragment.createInstance();
                    getSupportActionBar().setTitle(R.string.title_gallery);
                    break;
                case R.id.navigation_settings:
                    getSupportActionBar().setTitle(R.string.title_settings);
                    fragment = SettingsFragment.createInstance();
                    break;
            }
            if (fragment != null) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, mCurrFragment = fragment, fragment.getScreenTag())
                        .commit();
                return true;
            }
            return false;
        }
    };

    private ActivityMainBinding mBindingObject;
    private BaseFragment mCurrFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBindingObject = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(mBindingObject.toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (checkForPermissions(Constants.REQUEST_CHECK_APP_PERMISSIONS, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            initUI();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQUEST_CHECK_APP_PERMISSIONS) {
            for (Integer result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(mBindingObject.getRoot(), R.string.error_permissions_denied, Snackbar.LENGTH_LONG)
                            .setAction(R.string.btn_try_again, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    checkForPermissions(Constants.REQUEST_CHECK_APP_PERMISSIONS, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE);
                                }
                            })
                            .show();
                    return;
                }
            }
        }
        initUI();
    }

    public boolean checkForPermissions(int request, String... permissionsList) {
        if (Build.VERSION.SDK_INT >= 23) {
            //Check Location service permissions and request if needed
            List<String> permissions = new ArrayList<>();
            for (String permission : permissionsList) {
                if (ActivityCompat.checkSelfPermission(this, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(permission);
                }
            }
            if (permissions.size() != 0) {
                ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]),
                        request);
                return false;
            }
        }
        return true;
    }

    public void showProgressDialog(@StringRes int titleRes, @StringRes int messageRes) {
        showProgressDialog(getString(titleRes), getString(messageRes));
    }

    public Toolbar getScreenToolbar() {
        return mBindingObject.toolbar;
    }

    protected void showProgressDialog(String title, String message) {
        showProgressDialog(title, message, true);
    }

    protected void showProgressDialog(String title, String message, boolean modal) {
        ProgressDialog.newInstance(title, message)
                .setModal(modal).show(getSupportFragmentManager(), ProgressDialog.TAG);
    }

    protected void hideProgressDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            Fragment prev = fragmentManager.findFragmentByTag(ProgressDialog.TAG);
            if (prev != null) {
                ProgressDialog df = (ProgressDialog) prev;
                df.dismissAllowingStateLoss();
            }
        }
    }

    private void initUI() {
        if (mCurrFragment == null) {
            mBindingObject.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            mBindingObject.navigation.setSelectedItemId(R.id.navigation_gallery);
        }
    }
}
