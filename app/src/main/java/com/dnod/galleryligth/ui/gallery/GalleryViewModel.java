package com.dnod.galleryligth.ui.gallery;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;

import com.dnod.galleryligth.GalleryLightApp;
import com.dnod.galleryligth.data.Image;
import com.dnod.galleryligth.data.Settings;
import com.dnod.galleryligth.data.SettingsFactory;
import com.dnod.galleryligth.databinding.FragmentGalleryBinding;
import com.dnod.galleryligth.utils.Constants;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public final class GalleryViewModel extends ViewModel {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private FragmentGalleryBinding mBindingObject;
    private GalleryAdapter mAdapter;
    private GalleryAdapter.Listener mGalleryListener;
    private Settings mSettings;
    private Handler mUiHandler = new Handler();
    private LiveData<PagedList<Image>> mImagesData;
    private final Observer<PagedList<Image>> mImagesObservable = new Observer<PagedList<Image>>() {
        @Override
        public void onChanged(@Nullable PagedList<Image> images) {
            mAdapter.setList(images);
        }
    };

    void setBindingObject(FragmentGalleryBinding bindingObject) {
        this.mBindingObject = bindingObject;
        if (bindingObject != null) {
            loadData();
        } else {
            compositeDisposable.clear();
            mImagesData.removeObserver(mImagesObservable);
        }
    }

    public void setGalleryListener(GalleryAdapter.Listener listener) {
        this.mGalleryListener = listener;
    }

    private void loadData() {
        compositeDisposable.add(Schedulers.io().scheduleDirect(new Runnable() {
            @Override
            public void run() {
                List<Settings> settings = GalleryLightApp.getDataBase()
                        .getSettingsDao().getSettings(Constants.SETTINGS_NAME);
                if (!settings.isEmpty()) {
                    mSettings = settings.get(0);
                } else {
                    mSettings = SettingsFactory.createDefaultSettings();
                    GalleryLightApp.getDataBase().getSettingsDao().insert(mSettings);
                }
                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        init();
                        mImagesData = new LivePagedListBuilder(GalleryLightApp.getDataBase().getImageDao().getImagesByDate(),
                                new PagedList.Config.Builder()
                                        .setPageSize(mSettings.getPagingSize()).build()).build();
                        mImagesData.observeForever(mImagesObservable);
                    }
                });
            }
        }));

    }

    Set<Image> getSelectedItems() {
        return new HashSet<>(mAdapter.getSelectedItems());
    }

    void resetSelection() {
        mAdapter.resetSelections();
    }

    void updateGalleryColumns() {
        mBindingObject.gallery.setLayoutManager(new GridLayoutManager(mBindingObject.getRoot().getContext(), mSettings.getGalleryColumnsValue()));
    }

    void updateGalleryColumns(int count) {
        mBindingObject.gallery.setLayoutManager(new GridLayoutManager(mBindingObject.getRoot().getContext(), count));
    }

    private void init() {
        mBindingObject.gallery.setLayoutManager(new GridLayoutManager(mBindingObject.getRoot().getContext(), mSettings.getGalleryColumnsValue()));
        mBindingObject.gallery.setAdapter(mAdapter = new GalleryAdapter(mBindingObject.getRoot().getContext()));
        mAdapter.setListener(mGalleryListener);
    }
}
