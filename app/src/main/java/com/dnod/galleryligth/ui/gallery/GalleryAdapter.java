package com.dnod.galleryligth.ui.gallery;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dnod.galleryligth.R;
import com.dnod.galleryligth.data.Image;
import com.dnod.galleryligth.databinding.ItemGalleryBinding;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GalleryAdapter extends PagedListAdapter<Image, GalleryAdapter.Holder> {

    public interface Listener {
        void onSelectionListener(int count);
    }

    private final LayoutInflater layoutInflater;
    private final Set<String> selectedSet = new HashSet<>();
    private Listener mListener;

    GalleryAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.layoutInflater = LayoutInflater.from(context);
    }

    void resetSelections() {
        selectedSet.clear();
        notifyDataSetChanged();
    }

    void setListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder((ItemGalleryBinding) DataBindingUtil.inflate(layoutInflater,
                R.layout.item_gallery, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Image image = getItem(position);
        if (image != null) {
            Glide.with(layoutInflater.getContext())
                    .load(Uri.fromFile(new File(image.getPath())))
                    .apply(new RequestOptions().sizeMultiplier(0.5F))
                    .thumbnail(0.5f)
                    .into(holder.binding.iamge);
            if (selectedSet.contains(image.getPath())) {
                holder.binding.checkState.setVisibility(View.VISIBLE);
            } else {
                holder.binding.checkState.setVisibility(View.GONE);
            }
        }
    }

    public List<Image> getSelectedItems() {
        List<Image> result = new ArrayList<>();
        for (Image image : getCurrentList()) {
            if (selectedSet.contains(image.getPath())) {
                result.add(image);
            }
        }
        return result;
    }

    private void notifySelectionListener() {
        if (mListener != null) {
            mListener.onSelectionListener(selectedSet.size());
        }
    }

    final class Holder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {
        final ItemGalleryBinding binding;

        Holder(ItemGalleryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.iamge.setOnLongClickListener(this);
            this.binding.iamge.setOnClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            Image image = getItem(getAdapterPosition());
            if (!selectedSet.remove(image.getPath())) {
                selectedSet.add(image.getPath());
            }
            notifyItemChanged(getAdapterPosition());
            notifySelectionListener();
            return true;
        }

        @Override
        public void onClick(View view) {
            if (selectedSet.size() > 0) {
                Image image = getItem(getAdapterPosition());
                if (!selectedSet.remove(image.getPath())) {
                    selectedSet.add(image.getPath());
                }
                notifyItemChanged(getAdapterPosition());
                notifySelectionListener();
            }
        }
    }

    public static final DiffCallback<Image> DIFF_CALLBACK = new DiffCallback<Image>() {
        @Override
        public boolean areItemsTheSame(@NonNull Image oldImage, @NonNull Image newImage) {
            // User properties may have changed if reloaded from the DB, but ID is fixed
            return oldImage.getPath().equals(newImage.getPath());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Image oldImage, @NonNull Image newImage) {
            // NOTE: if you use equals, your object must properly override Object#equals()
            // Incorrectly returning false here will result in too many animations.
            return oldImage.getPath().equals(newImage.getPath());
        }
    };
}
