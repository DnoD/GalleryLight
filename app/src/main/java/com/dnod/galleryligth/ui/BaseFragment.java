package com.dnod.galleryligth.ui;

import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {

    public final void showProgressDialog(String title, String message) {
        ((MainActivity) getActivity()).showProgressDialog(title, message);
    }

    public final void showProgressDialog(@StringRes int titleRes, @StringRes int messageRes) {
        ((MainActivity) getActivity()).showProgressDialog(titleRes, messageRes);
    }

    public final void hideProgressDialog() {
        ((MainActivity) getActivity()).hideProgressDialog();
    }

    public abstract String getScreenTag();
}
