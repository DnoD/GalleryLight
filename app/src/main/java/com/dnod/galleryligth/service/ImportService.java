package com.dnod.galleryligth.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;

import com.dnod.galleryligth.GalleryLightApp;
import com.dnod.galleryligth.data.Image;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ImportService extends IntentService {
    private static final String PROVIDED_DATA = "provided_uris";

    public static Intent createIntent(Context context, @NonNull ArrayList<Uri> uris) {
        Intent intent = new Intent(context, ImportService.class);
        intent.putParcelableArrayListExtra(PROVIDED_DATA, uris);
        return intent;
    }

    public ImportService() {
        super("ImportService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //Remove all stored data before import new values
        GalleryLightApp.getDataBase().getImageDao().clear();
        ArrayList<Uri> uris = intent.getParcelableArrayListExtra(PROVIDED_DATA);
        List<Image> images = new ArrayList<>(uris.size());
        for (Uri uri : uris) {
            images.add(createImageFromUri(uri));
        }
        GalleryLightApp.getDataBase().getImageDao().insert(images);
    }

    private Image createImageFromUri(Uri uri) {
        String[] projection = new String[]{MediaStore.MediaColumns.DATA, MediaStore.Images.Media.DISPLAY_NAME};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        cursor.moveToFirst();
        int pathIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
        int nameIndex = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
        Image image = new Image();
        image.setDate(Calendar.getInstance().getTime());
        image.setName(cursor.getString(nameIndex));
        image.setPath(cursor.getString(pathIndex));
        cursor.close();
        return image;
    }
}
