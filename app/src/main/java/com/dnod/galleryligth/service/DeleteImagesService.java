package com.dnod.galleryligth.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.dnod.galleryligth.GalleryLightApp;
import com.dnod.galleryligth.data.Image;

import java.util.ArrayList;

public class DeleteImagesService extends IntentService {
    private static final String PROVIDED_DATA = "provided_uris";

    public static Intent createIntent(Context context, @NonNull ArrayList<Image> images) {
        Intent intent = new Intent(context, DeleteImagesService.class);
        intent.putParcelableArrayListExtra(PROVIDED_DATA, images);
        return intent;
    }

    public DeleteImagesService() {
        super("DeleteImagesService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ArrayList<Image> images = intent.getParcelableArrayListExtra(PROVIDED_DATA);
        GalleryLightApp.getDataBase().getImageDao().delete(images);
    }
}
