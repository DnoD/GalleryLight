package com.dnod.galleryligth.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.dnod.galleryligth.GalleryLightApp;

public class ClearDBService extends IntentService {

    public static Intent createIntent(Context context) {
        return new Intent(context, ClearDBService.class);
    }

    public ClearDBService() {
        super("ClearDBService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GalleryLightApp.getDataBase().getImageDao().clear();
        //TODO: Clear all related data like stored files and remove them from ContentProvider
    }
}
